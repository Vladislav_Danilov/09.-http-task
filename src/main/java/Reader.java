import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Reader {

  /*public File getResponceToYandex(File path) throws IOException {
    String USER_AGENT =
        "user-agent:Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/48.0.2564.116 Safari/537.36";
    String url = "https://yandex.ru/maps";
    HttpClient client = HttpClientBuilder.create().build();
    HttpGet request = new HttpGet(url);
    request.addHeader("User-Agent", USER_AGENT);
    HttpResponse response = client.execute(request);
    PrintWriter out = new PrintWriter(path.getAbsoluteFile());
    BufferedReader rd =
        new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
    String line = "";
    while ((line = rd.readLine()) != null) {
      out.print(line);
    }
    rd.close();
    out.close();
    return path;
  }*/

  public File getCsrfToken(File input, File output) {
    String temp = "";
    BufferedReader in;
    try {
      in = new BufferedReader(new FileReader(input.getAbsoluteFile()));
      String line = "";
      while ((line = in.readLine()) != null) {
        temp = temp + line;
      }
      in.close();
    } catch (IOException e) {
      e.printStackTrace();
    }
    Pattern p = Pattern.compile("(\\\"csrfToken\\\"\\:\\\"([a-z0-9]{1,})\\:([a-z0-9]{1,})\\\")");
    Matcher m = p.matcher(temp);
    try {
      PrintWriter out = new PrintWriter(output.getAbsoluteFile());
      while (m.find()) {
        out.print(m.group(1));
        out.println();
      }
      out.close();
    } catch (FileNotFoundException e) {
      e.printStackTrace();
    }
    return output;
  }

  public File getYandexuid(File input, File output) {
    String temp = "";
    BufferedReader in;
    try {
      in = new BufferedReader(new FileReader(input.getAbsoluteFile()));
      String line = "";
      while ((line = in.readLine()) != null) {
        temp = temp + line;
      }
      in.close();
    } catch (IOException e) {
      e.printStackTrace();
    }
    Pattern p = Pattern.compile("(yandexuid\\=([0-9]{1,}))\\;");
    Matcher m = p.matcher(temp);
    try {
      PrintWriter out = new PrintWriter(output.getAbsoluteFile());
      while (m.find()) {
        out.print(m.group(1));
        out.println();
      }
      out.close();
    } catch (FileNotFoundException e) {
      e.printStackTrace();
    }
    return output;
  }
  
  public File getCoordinate(File input, File output) {
    String temp = "";
    BufferedReader in;
    try {
      in = new BufferedReader(new FileReader(input.getAbsoluteFile()));
      String line = "";
      while ((line = in.readLine()) != null) {
        temp = temp + line;
      }
      in.close();
    } catch (IOException e) {
      e.printStackTrace();
    }
    Pattern p = Pattern.compile("(InternalToponymInfo\\\"\\:\\{.*?\\})");
    Matcher m = p.matcher(temp);
    try {
      PrintWriter out = new PrintWriter(output.getAbsoluteFile());
      while (m.find()) {
        out.print(m.group(1));
        out.println();
      }
      out.close();
    } catch (FileNotFoundException e) {
      e.printStackTrace();
    }
    return output;
  }

}

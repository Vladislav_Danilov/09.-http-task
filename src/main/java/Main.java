import java.io.File;
import java.io.IOException;

public class Main {
  public static void main(String[] args) throws IOException {
    Reader reader = new Reader();
    reader.getCsrfToken(new File("src/main/resources/out.txt"),
        new File("src/main/resources/csrfToken.txt"));
    reader.getYandexuid(new File("src/main/resources/out.txt"),
        new File("src/main/resources/yandexuid.txt"));
    reader.getCoordinate(new File("src/main/resources/coord.txt"),
        new File("src/main/resources/complete.txt"));
  }
}
